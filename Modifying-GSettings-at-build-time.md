### Explanation

Our issue is that the original simulated resolution does not imitate a phone-like experience. After manual exploration, we realised the problem is the text-scaling-factor and its default 1.0 value. Further experimentation revealed that altering this value to 2.0 was ideal.

The text-scaling-factor gets controlled by GSettings, which gets described conceptually as:

- GSettings are a nice way to manage your application’s settings.
- This system can be seen as a database, whose schema is described in an XML file (conventionally named .gschema.xml) which is then compiled to improve runtime performance.

[The official site is here.](https://wiki.gnome.org/HowDoI/GSettings)

### Override files
We can define a `.override` file to replace the xml values for text scaling. Begin by creating a new directory in files called `gsettings` then adding updating text scaling can be done by creating a file `01_mobile.gschema.override` with contents
```
[org.gnome.desktop.interface]
text-scaling-factor=1.5
scaling-factor=1
```

### Importing override
Creating a new custom element `elements/custom/gsettings.bst` we can import our override and recompile the gsettings schema by adding the contents:

```
kind: import

sources:
- kind: local
  path: elements/custom/gsettings/src/01_mobile.gschema.override

config:
  source: /
  target: /usr/share/glib-2.0/schemas/

  public:
    bst:
      integration-commands:
      - glib-compile-schemas usr/share/glib-2.0/schemas
```

### Add dependency

We need to include this file into the image this can be done editing `deps.bst` to include
```
- custom/xml_editor/xml_editor.bst
```