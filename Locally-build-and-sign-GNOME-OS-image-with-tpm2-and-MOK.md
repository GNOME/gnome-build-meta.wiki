Originally from https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2418

* Download and install image
  * For example from ISO
  * Or as VM with `./utils/run-secure-vm.sh --buildid 582348`
* Remember to enroll a recovery password on first boot:
  ```
  sudo systemd-cryptenroll --unlock-key-file=/run/recovery-password --password --wipe-slot=recovery /dev/disk/by-partlabel/root
  sudo systemd-cryptenroll --unlock-key-file=/run/recovery-password --password --wipe-slot=recovery /dev/disk/by-partlabel/home
  ```
* Create your local keys
  * The following command will create keys for local build and use snake oil keys where not important: 
    ```
    make KEY_ID="Valentin David" IMPORT_MODE=local -C files/boot-keys/
    ```
  * You may need to run `make clean -C files/boot-keys/`
* Copy the gpg key for downloads
  * `files/boot-keys/import-pubring.gpg` from build should be copied to `/etc/systemd/import-pubring.gpg` (Note, would be better to append what is already in `/usr/lib/systemd`)
    ```
    sudo cp files/boot-keys/import-pubring.gpg /etc/systemd/
    ```
* Enroll your mok key
  * Find your mok key at `files/boot-keys/VENDOR.der` from your build
  * `sudo mokutil --import VENDOR.der`, set a password
  * Reboot, and enroll the key with that password.
* Modify sysupdate.d, adjust the address (10.0.2.2 from a VM, localhost from the same machine, or the name of the machine with the build)
  ```
  sudo cp -r /usr/lib/sysupdate*.d /etc
  sudo sed -i 's,Path=https://.*,Path=http://10.0.2.2:8080,' /etc/sysupdate*.d/*.conf
  # or
  sudo sed -i 's,Path=https://.*,Path=http://10.0.2.2:8080,' /etc/sysupdate*.d/*.conf
  ```
* Run a local server
  * In the build, run `./utils/run-sysupdate-repo.sh`. Eventually with `--devel` if all components are needed.
    ```
    ./utils/run-sysupdate-repo.sh --devel
    ```
* Update to the local version.
  * `sudo /usr/lib/systemd/systemd-sysupdate list` to list the available versions.
  * Update to l.1 for example.
    ```
    sudo /usr/lib/systemd/systemd-sysupdate update l.1 && sudo /usr/lib/systemd/systemd-sysupdate update --component devel l.1
    ```
* Reboot, select the right kernel, and type the recovery password
* Enroll new tpm signature
  * `sudo systemd-cryptenroll --wipe-slot=tpm2 --tpm2-device=auto /dev/disk/by-partlabel/root`
  * `sudo systemd-cryptenroll --wipe-slot=tpm2 --tpm2-device=auto /dev/disk/by-partlabel/home`
* You are set!
  * Eventually set default kernel with `bootctl set-default`.
