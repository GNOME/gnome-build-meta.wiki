### Intuition

Python is an interpreted language, i.e, there is no compilation involved. Python converts source code written by the developer into an intermediate language which is again translated into native machine code that is executed. With Python you have two options in terms of the elements you can use:

1. Import element
2. Distutils element

Import elements are basic elements that enable you to produce artefacts directly from their sources without any kind of processing. Distutils elements are the de-facto standard method for using python distutils, however, this tends to be overkill for simple scripts. This tutorial focuses on using import elements and Python scripts. In the future, we may add a simple tutorial for the latter.

Additional reading links:

1. https://github.com/apache/buildstream/blob/master/src/buildstream/plugins/elements/import.py
2. https://docs.buildstream.build/1.6/elements/distutils.html?highlight=dist#module-elements.distutils

### Commands to run

Run the following commands to create the necessary files and directories assuming you are in the root directory of the GnomeOS project:

```
mkdir elements/custom
mkdir elements/custom/hello_python
mkdir elements/custom/hello_python/src
touch elements/custom/hello_python/hello_python.bst
touch elements/custom/hello_python/src/hello_python.py
```

Run the following command to create our Python program. We intend to provide a basic Hello World example.

```
echo '
# hello_python.py - Simple hello world program

if __name__ == "__main__":
    print("Hello, World!")
'\
> elements/custom/hello_python/src/hello_python.py
```

Run the following command to create our import element which defines the commands that Buildstream should run processing this .bst file. The integrated comments should be enough to explain the basic logic of the import element.

```
echo '
kind: import

# Stage the files/src directory for building
sources:
  - kind: local
    path: elements/custom/hello_python/src

# The import element simply stages the given sources
# directly to the root of the sandbox and then collects
# the output to create an output artefact.
config:

  # By default we collect everything staged, specifying a
  # directory here to output only a subset of the staged
  # input sources.
  source: /

  # Prefix the output with an optional directory, by default
  # the input is found at the root of the produced artefact.
  target: /bin
'\
> elements/custom/hello_python/hello_python.bst
```

The final step is to link the hello_python.bst file we created into the dependency chain so that Buildstream is aware of our new file and can process the commands. As mentioned, we wish to define such a dependency as very low in the chain so that the prior cache can be used and that the majority of elements do not have to be rebuilt from the cache. A recommended location is <deps.bst>.

Run the following command to define said dependency.

```
echo "
- custom/hello_python/hello_python.bst
"\
>> elements/vm/deps.bst
```

### Building the image

Build using the tutorial [here](Preliminary%20build%20instructions)

### Running the image

Run using the tutorial for your architecture \[x86_64\]Preliminary build instructions#for-qemu-x86_64) or [aarch64](Preliminary%20build%20instructions#for-pinephone-or-qemu-aarch64)

You should be able to find the new executable inside the root directory of the OS inside the bin folder i.e. run the following inside GnomeOS.

```
cd /
python ./bin/hello_python.py
```

### Evidence of hello_python.py compiled and running in the final image

![mTBAeT2](https://i.imgur.com/mTBAeT2.png)