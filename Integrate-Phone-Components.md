This is a tutorial for adding phone components. As an example, these three phone components will be added: 'calls' and 'feedbackd'.

## Adding elements

**To add the elements, place the file path to the phone component you want into ‘deps.bst’:**

For Calls and Feedbackd, add:

```
  - core/calls.bst
  - core-deps/feedbackd.bst
```

## Tracking files

**Tracking files, if needed:**

To track things during the build, type `bst track <bst file>` into terminal, then build again.

For our examples, both `core/calls.bst` and `core-deps/feedbackd.bst` did not need to tracking.

Continue to [building](Preliminary%20build%20instructions).

## Test

Within the phone's app screen you should now see the calls app.

To test this typing `fbcli` into terminal can simulate a phonecall

```
$ fbcli
Triggering feedback event for event 'phone-incoming-call'
Press <RETURN> to end feedback right away. 

```