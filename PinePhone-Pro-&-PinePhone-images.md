## Adding components

- [Integrate Phone Components](Integrate%20Phone%20Components)
- [Integrating iputils and glib-networking](Integrate%20iputils%20and%20glib%20networking)
- [Compiling C programs](Compiling%20C%20programs%20into%20GNOME%20OS)
- [Adding Python scripts](Adding%20Python%20scripts%20into%20GNOME%20OS)
- [Modifying GSettings at build time](Modifying%20GSettings%20at%20build%20time)

## Image generation

- [Preliminary build instructions](Preliminary%20build%20instructions)

## Running GNOME OS

- [Booting on PinePhone Pro & PinePhone](Booting-on-Pinephone-Pro-&-PinePhone)
- [Emulating GNOME OS on the PinePhone Pro & PinePhone](Emulating-GNOME-OS-on-the-PinePhone-Pro-&-PinePhone)
- [SSH into PinePhone Pro & PinePhone](SSH-into-PinePhone-Pro-&-PinePhone)

## Atomic Upgrades
- [Upgrading GNOME OS](Upgrading GNOME OS)
  - [CLI](https://gitlab.gnome.org/tanvirroshid/gnome-build-meta/-/wikis/Upgrading-GNOME-OS#cli)
  - [GNOME Software](https://gitlab.gnome.org/tanvirroshid/gnome-build-meta/-/wikis/Upgrading-GNOME-OS#gnome-software)