### Connect to WiFi
After finishing the initial setup connect to WiFi either through the GUI or by nmcli
```
sudo nmcli dev wifi connect <network-ssid> password <network-password>
```

### Enable sshd
In a terminal either on the device or through serial:
```
sudo systemctl enable sshd
sudo systemctl start sshd
```

### Connect
On the device you want to use for connecting, open a console window and enter:
```
ssh <username>@<ipv4 address>
# Example
ssh myuser@1.2.3.4
```