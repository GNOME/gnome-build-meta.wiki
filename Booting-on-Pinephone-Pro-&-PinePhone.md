## Boot from internal storage

1. Download [Jumpdrive](https://github.com/dreemurrs-embedded/Jumpdrive/releases/) for your device e.g. PinePhone
2. Use flash the SD card with the Jumpdrive image
3. Insert the SD card, and connect the PinePhone via USB to your machine, and allow to boot
4. Once prompted with a screen on the PinePhone that says `Jumpdrive is running` you should see two USB devices on your computer
5. Flash the GNOME OS image to the phones eMMC storage
6. Once finished turn off the phone, remove the Jumpdrive SD card, and allow to boot
7. If after multiple boot attempts or no [serial output](Booting%20on%20Pinephone%20from%20GnomeOS%20image#diagnostics) attempt to use [Tow-Boot](Booting%20on%20Pinephone%20from%20GnomeOS%20image#using-tow-boot)

## Boot from SD card

1. Flash the GNOME image to the SD card
2. Once finished insert the SD card, turn on the phone, hold the volume-down button **and release when an aqua light colour appears** and allow to boot
3. If after multiple boot attempts or no [serial output](Booting%20on%20Pinephone%20from%20GnomeOS%20image#diagnostics) attempt to use [Tow-Boot](Booting%20on%20Pinephone%20from%20GnomeOS%20image#using-tow-boot)

## Debugging PinePhone boot using UART cable

:notebook: Minicom is required install by `apt-get install minicom`

The [UART cable](https://wiki.pine64.org/index.php/PinePhone#Serial_console) plugs into the PinePhone's headphone port. The hardware switch for headphone (number 6) needs to be disabled to enable serial console.

### Minicom

- To get the the serial device the UART cable is connected to run `ls /dev | grep ttyUSB` you will get a result such as `ttyUSB0`
- Open minicom with `sudo minicom -D /dev/ttyUSBX -s -b 115200`
  - `-D` specifies the device
  - `-s` enters setup mode (only needed first time)
  - `-b` specifies the baudrate
- Navigate using arrow keys to `Serial port setup`
- Ensure `Hardware Flow Control` is disabled and the baudrate is as specified
  - `Esc` to return to previous menu
- Navigate to `Save setup as dfl`
- Navigate to `Exit`
- The console window will now output from the serial device

## Using Tow-Boot

If you have flashed an image you're expecting to boot with no output to console it is possible the bootloader in the image is incompatible in some way. We can replace the bootloader with [Tow-Boot](https://github.com/Tow-Boot/Tow-Boot/releases) we can do this by:

- Download the correct tar for your file
- `unxz` and `tar xf` or use archive manager and extract folder
- Flash `mmcboot.installer.img` to an SD card
- Insert the SD card and boot the phone
- Go through the menu and install Tow-Boot to internal eMMC storage
- Power off remove the SD card and allow to boot