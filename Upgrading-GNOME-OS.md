## Introduction

The aim of this tutorial is to show users how to upgrade their Operating System after an atomic update. 

## Server side

Please run the following to setup the server after building the new image:
```
./utils/update-local-repo.sh --user
./utils/run-local-repo.sh
```

# CLI
## Client side

```
sudo qemu-img resize -f raw disk.img +30G
```

The resize command gives us a nice 30GB disk area for use before the first boot so the partitions are automatically adjusted. Then [run](Emulating GNOME OS).

### Running the atomic update
Run the following commands inside the home directory of your terminal to setup the upgrade script:
```
touch atomic.sh
chmod +x atomic.sh
echo '
#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if [ "$#" != "2" ]; then
    echo "Error: usage is <sudo atomic.sh reference ip/>."
    exit
fi

NAME="workstation"
REF="$1"

# Fetching the repo from another machine in your local network
URI="$2"
GPG="$2/key.gpg"

sudo enable-developer-repository \
    --name "$NAME" \
    --url "$URI" \
    --gpg "$GPG" \
    --ref "$REF"
' > atomic.sh
```

Once completed please run:
```
./atomic.sh gnome-os/43/x86_64-user IP
```

Where IP must be substituted with the IP address from your server's ip. You should be able to see a download occurring and a series of GET requests on the server side. You must specify the protocol and port of the server, i.e., the format for IP is: `http://address:8000`.

# GNOME Software
First, we need to grab the IP address of the server then we need the program [sed](https://www.gnu.org/software/sed/manual/sed.html) in order to perform our edits programmatically.

To edit the URL Gnome Software points to, run the following:
```
sed -i '/url=OSTREE_REMOTE_URL/c\url=IP' files/vm/ostree-config/gnome.conf.in
```

The IP must be in the format `http://address:8000`. Assuming you haven't edited the server, the default port for the server should be port `8000`.

Our second step is to update the Public Key that our client side's image is built with.

To update the public key run:
```
echo "KEY" > files/vm/gnome.gpg
```

The public key is present on the server, which should be in the location: `ostree-repo/key.gpg`.

You can also run the following command to get the key from the server in a more automated fashion:
```
curl http://address:8000/key.gpg
```