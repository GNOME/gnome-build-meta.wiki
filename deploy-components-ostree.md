Build on a machine:

```shell
bst track --deps=all vm/repo-devel.bst
bst build vm/repo-devel.bst

# Setup and update ostree repo
./utils/update-local-repo.sh --devel

# Run a webserver to serve it
# XXX: would be cool if we could get this to run on the back ground
./utils/run-local-repo.sh

# If you have already the repo setup on the client machine, upgrade and reboot with ssh
# To get ssh to work refer here: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/347#note_1046112
ssh deploy_machine -C 'sudo ostree admin switch --reboot workstation:gnome-os/master/x86_64-devel'
```

Setup ostree repo on the client/deploy machine

```shell
NAME="workstation"
REF="gnome-os/master/x86_64-devel"

# Fetching the repo from another machine in your local network
URI="http://192.168.2.2:8000"
GPG="http://192.168.2.2:8000/key.gpg"

# If gnome-os is runnuning under qemu/Boxes use the following uri
URI="http://10.0.2.2:8000/"
GPG="http://10.0.2.2:8000/key.gpg"

# If you are self-hosting, deploying on the machine you are building,
# use localhost
URI="http://127.0.0.1:8000/"
GPG="http://127.0.0.1:8000/key.gpg"

sudo enable-developer-repository \
    --name "$NAME" \
    --url "$URI" \
    --gpg "$GPG" \
    --ref "$REF"
    
```

To switch back to the Nightly tree:

`sudo ostree admin switch --reboot gnome-os:gnome-os/master/x86_64-devel`