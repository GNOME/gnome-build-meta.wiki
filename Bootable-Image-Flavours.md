
# Table of Contents

1.  [Basic VMs](#org6da79eb)
2.  [Secure VMs](#orgb57d6ff)
3.  [Installer ISOs](#orgb67eb89)
4.  [Hacking on a Component](#org9a2bfd0)

gnome-build-meta contains build metadata for several virtual machine types:

-   "Basic" VMs (in `vm/`)
-   "Secure" VMs (in `vm-secure/`)
-   Installer ISO images (in `iso/`)
    -   This can use either the basic or secure VM as the installed system


<a id="org6da79eb"></a>

# Basic VMs

The basic VM images use [ostree](https://ostreedev.github.io/ostree/introduction/) for updates and include some niceties not present
in the secure variant, such as fish, git LFS and a graphical boot.

You can build and check out these images by running

```bash
    # Build the image
    bst build vm/image.bst
    
    # Checkout the finished raw disk image
    bst artifact --hardlinks checkout vm/image.bst --directory image/
```

This can be run in GNOME Boxes by importing the local image.


# Secure VMs

Secure VM images are more experimental, providing an image-based immutable
operating system based on modern conventions from the [UAPI specifications](https://uapi-group.org/specifications/), such
as UKIs, extension images and discoverable dynamic partitions.

The functionality for this is largely found in modern systemd features, in
particular we use:

-   **`systemd-repartd`:** Dynamic partition sizing
-   **`systemd-sysext`:** Extension images for optional components
-   **`systemd-sysupdate`:** Updates and A/B booting
-   **`systemd-ukify`:** Generation of UKI

The images are called "secure" as they use secure boot with TPM support.

Building secure VM images is a little more involved.  First we need to generate
some keys to sign the images with, in order to have secure boot work.  Once
that's done we can build and checkout the image.

```bash
    # Generate keys for secure boot
    make -C files/boot-keys generate-keys IMPORT_MODE=snakeoil
    
    # Build the image
    bst build vm-secure/image.bst
    
    # Checkout the finished raw disk image
    bst artifact checkout --hardlinks vm-secure/image.bst --directory image/
```

Running the raw image is some work, as we need to have secure boot working.  I
recommend using the script `./utils/run-secure-vm.sh`, which will handle the setup
of a virtual TPM with the keys added.

```bash
    # Run secure VM with a serial line on this TTY for debug
    ./utils/run-secure-vm.sh --debug
```

# Installer ISOs

The installer ISO creates an image that can be used as installation media to
install GNOME OS on a physical machine or VM.  This can use either of the
variants for the underlying image.  By default it uses the "basic" image, but by
passing an option to BuildStream we can use the secure image instead.

```bash
    # Build and checkout an ISO using the base VM
    bst build iso/image.bst
    bst artifact checkout --hardlinks iso/image.bst --directory base-iso/
    
    # Build and checkout an ISO using the secure VM
    make -C files/boot-keys generate-keys IMPORT_MODE=snakeoil
    bst -o secure_image_installer true build iso/image.bst
    bst -o secure_image_installer true artifact checkout --hardlinks iso/image.bst --directory secure-iso/
```

These images may be run in GNOME Boxes.

# Hacking on a Component

When developing low-level libraries used in the booting images, it can be
cumbersome to have a good development workflow.  In particular, if systemd
requires work then it can be difficult to test changes without incurring hours
of building.  Thankfully with some BuildStream wrangling we can reduce this
cycle time to close to 30 minutes.

To "quickly" test a component change, follow the steps below (replacing the
element of choice).

```bash
    # Open a BuildStream "workspace" for the component
    bst workspace open core-deps/systemd.bst
    
    # <Make changes to things in core-deps/systemd/>
    
    # Build the component alone
    bst build core-deps/systemd.bst
    
    # Remove cached state of the VM
    bst artifact delete vm-secure/*.bst2
    
    # Rebuild the full image using non-strict mode to avoid rebuilding dependencies
    bst --no-strict build vm-secure/image.bst
    
    # Run the image
    ./utils/run-secure-vm --debug --reset
```

Some of these commands may have issues.  If `bst artifact delete` fails with a
large stack trace with error messages about "missing CAS objects", you can
remove references to the artifact manually:

```bash
    rm -rf "${HOME}/.cache/buildstream/refs/gnome/element-path-with-slash-replaced-with-hyphen/*"
```
