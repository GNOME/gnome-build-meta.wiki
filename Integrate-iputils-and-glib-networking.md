# Integration of iputils:

- [iputils.bst found here](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/elements/components/iputils.bst)
- Added it to deps.bst
- Evidence it added in screenshot below, showing before and after typing arping, clockdiff, ping or tracepath into terminal:![UgcpoWU](https://i.imgur.com/UgcpoWU.png)

# Integration of glib-networking:
- add `sdk/glib-networking.bst` to deps.bst file