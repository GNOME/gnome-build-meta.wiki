Using openQA we can install the nightly GNOME OS images in a virtual machine,
and simulate user interactions to test that it boots to a working graphical
desktop. We can then do more integration testing of GNOME as a whole via the
Shell and the core applications.

openQA has some useful online documentation. Be sure to look at the
[starter guide](http://open.qa/docs/#gettingstarted),
[tests developer guide](http://open.qa/docs/#writingtests) and
[users guide](http://open.qa/docs/#usersguide).

Deployment of openqa.gnome.org is documented [here](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/openqa/Deployment).

Documentation in this page:

[[_TOC_]]

## Status of openQA testing

The openQA testing project is a work in progress and can be considered "in beta". As of April 2023 it is maintained by Sam Thursfield on a voluntary, best-effort basis. The tests are running against [gnome-build-meta master branch](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/pipelines) with `allow_failure: true`, so test failures can be ignored.

An incomplete list of requirements for this to become production-ready:

  * A commitment from at least 3 more individuals and/or at least 1 business to indefinite, ongoing maintenance of the tests and OpenQA infrastructure 
  * Buy-in from wider GNOME designer + developer teams to update tests in line with design and app changes.
  * A way to update all screenshots in line with global theme changes: https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/1

## Checking test results

1. Look at the latest 'master' pipelines for gnome-build-meta: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/pipelines?page=1&scope=all&ref=master

2. Open the 'test-s3-image' job:

![image](uploads/81de9378eec4a4857ff5c5cbf8dae83b/image.png)

3. Click the link in the log to open the OpenQA web UI:

![image](uploads/dcca54491c5993122a8fbd3539fe02f6/image.png)

If you are looking at a specific openQA test job and want to find the associated Gitlab CI job, you can do this too:

  * Go to "Logs and Assets" and open `vars.json`
  * Look for `GITLAB_CI_JOB_URL`

## Running the openQA tests in CI

The tests run automatically for two projects.

  * In [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/), the GNOME project integration repo
  * In [openqa-tests](https://gitlab.gnome.org/GNOME/openqa-tests), where the tests themselves are kept

There are currently two ways to manually trigger the tests in Gitlab CI.

One, is to trigger a [gnome-build-meta pipeline](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/pipelines) against the 'master' branch or a release tag. This builds an image from scratch which is [slow](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/396).

The other is to trigger a pipeline in the [openqa-tests](https://gitlab.gnome.org/GNOME/openqa-tests) repo. This will run against the current [os.gnome.org](https://os.gnome.org/) image, which is fast. This is very useful when developing new tests.

It's not currently possible to run the openQA tests against regular merge requests to gnome-build-meta. See https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/601 to see what is needed.

See below for instructions on running the test suite on your local machine.

## When tests fail

Tests can fail for several reasons. You will see the failure in the Web UI at https://openqa.gnome.org/, and the corresponding Gitlab CI job will also fail.

First check the "Logs & Assets" tab. Useful files:

  * `autoinst-log.txt` - log of installing the ISO and running the testcases - QEMU issues and bugs in the tests will appear here
  * `serial0.txt` - serial console capture which includes [most](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/17) of the systemd journal logs too
  * `video.ogv` - a live recording of the display output

If you see "no candidate needle matched" or "`assert_screen` failure", this indicates a graphical change meaning the screenshot no longer matches. If the UI change is intentional then we will need to update the needle.

The OpenQA Web UI lets you compare the actual screenshot with the expected ones. Select a needle in 'Candidate needles and tags', then use the orange slider to see differences. The 'landscape' and 'eye' icons in the list switch between area-only or full diff. In the screenshot below, we have the expected image (installation completed) on the left, and the actual screenshot (installation still in progress) on the right.

![image](uploads/6020c33f597fbf516fed756541f2f2c2/image.png)

If you think the test failure represents a bug, open an issue against the relevant component. Otherwise, or if in doubt, [open a gnome-build-meta issue](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues).

## How to update a needle

Test scripts live in [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/) in the `openqa/` folder. If you need to modify the test itself, create a branch of gnome-build-meta as normal with your change. The test pipeline will run with your version of the tests, and once it passes we can merge this to master.

Needles live in [openqa-needles](https://gitlab.gnome.org/GNOME/openqa-needles) folder, and the `master` branch is used by all tests. Multiple needles can have the same name, and OpenQA will pass a test if any one needle matches. If you need to modify a needle, the workflow is to create a new one, and the preferred way is to use OpenQA's [developer mode](http://open.qa/docs/#_developer_mode) as follows.

 1. Ensure you have 'operator' permissions on OpenQA - ask an admin or open an issue if you don't have this.
 2. Open the failed test in the Web UI.
 3. Click the 'Create new needle' button (pin with a green plus) to open the developer mode page.
 4. Use the 'Screenshot and Areas' section to create a new needle
    * Usually you 'Take image from' the screenshot and 'Copy areas from' the most recent needle.
    * Click and drag on the screenshot to create, move and resize match areas
    * Click an area once to select it, enabling the "Selected area" controls
    * Click on area again to change its [type](http://open.qa/docs/#_areas) - 'exclude' matches can be very useful when updating needles for UI changes.
 5. Now use 'Basics of Needle' to commit your change to [openqa-needles.git](https://gitlab.gnome.org/GNOME/openqa-needles):
    * Write a short commit message describing why you made the change
    * Click 'Save' to commit and push from the OpenQA webUI.

Now rerun the test which hopefully passes.

## Adding more tests

OpenQA can be used to test anything that
[gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/) builds.
We must be able to maintain the tests with volunteer effort, so try to write
tests only around parts of the UI that won't change frequently.

The [how to write tests](http://open.qa/docs/#_how_to_write_tests)
documentation is your starting point for adding a new test.

Some distributions are already testing GNOME with OpenQA, and you may find you
can adapt an existing test. Here are some we know of:

  * openSuSE: [web UI](http://openqa.opensuse.org/), [tests](https://github.com/os-autoinst/os-autoinst-distri-opensuse), [needles](https://github.com/os-autoinst/os-autoinst-needles-opensuse)
  * Fedora: [web UI](https://openqa.fedoraproject.org/), [tests](https://pagure.io/fedora-qa/os-autoinst-distri-fedora/blob/master/f/tests)

## Running the test suite locally

When adding or debugging tests it's convenient to run the tests on your local machine, instead of on a Gitlab CI runner. There are two ways to do this. In both cases, you first need to clone the tests and fetch the ISO as follows:

1. Clone [openqa-tests](https://gitlab.gnome.org/GNOME/openqa-tests/) repo.

2. Clone [openqa-needles](https://gitlab.gnome.org/GNOME/openqa-needles/) repo, link or rename it to be inside openqa-tests folder at path `./needles`.

3. Get the test media that you need. There are currently two artifacts published for each GNOME OS build and the end-to-end tests use both:

  * **iso**: an installer image, used by `gnome_install` testsuite
  * **disk**: a pre-installed disk image, used by the other testsuites

Stable images are available from https://os.gnome.org/:

  * iso: https://os.gnome.org/download/latest/gnome_os_installer.iso
  * disk: https://os.gnome.org/download/latest/gnome_os_disk.img.xz

Each successful CI pipeline for gnome-build-meta updates artifacts with different URLs. Use the script [./utils/test_media_url.py](https://gitlab.gnome.org/GNOME/openqa-tests/-/blob/master/utils/test_media_url.py) to locate them.

### Using ssam_openqa tool

The [ssam_openqa](https://gitlab.gnome.org/sthursfield/ssam_openqa/) tool wraps the above steps in a commandline program. Follow the [instructions in the README](https://gitlab.gnome.org/sthursfield/ssam_openqa/#quickstart-for-gnome-testing) to get started.

### Using Docker or Podman directly

You will need Docker or Podman to run the tests, and need to have the `jq` utility installed.

1. Look at https://openqa.gnome.org/ for the last time this test ran. Download the `vars.json` file, e.g. https://openqa.gnome.org/tests/1024/file/vars.json

2. Edit the file `vars.json` and remove the `BIOS` setting. This otherwise breaks the backend.

3. Run this command inside the `openqa-tests` clone to start the openqa_worker image. You will need to modify the `-v` flags to match the assets needed by the test, e.g. mounting the disk image instead of the ISO if the test requires it.

       podman run --name openqa --privileged -it \
           -v .:/tests -v ./installer.iso:/installer.iso \
           --entrypoint isotovideo registry.opensuse.org/devel/openqa/containers15.5/openqa_worker:latest  \
           $(jq -jr 'keys[] as $k | "\($k)=\(.[$k])\n"'  < config/vars.json) \
           CASEDIR=/tests NEEDLES_DIR=/tests/needles TEST=gnome_install

    The test log will be written to stdout, the other data is still inside the container.

4. Copy the test results and video out of the container:

       podman cp openqa:/testresults ./testresults 
       podman cp openqa:/video.ogv .
       # See all files in the container by running: podman export 12345678abc | tar -v -t | less

5. Remove the container with `podman rm openqa` when you are done.

It's common to use `save_screenshot` when writing testcases, which gives you a `.png` which you can then convert into a Needle.

See also: https://kalikiana.gitlab.io/post/2022-03-16-running-standandalone-tests-with-isotovideo/

## Future work

See [openqa-tests issues](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues) and  [open gnome-build-meta issues matching 'openqa'](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues?scope=all&state=opened&search=openqa)