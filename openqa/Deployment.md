# openqa.gnome.org deployment

This server was initially deployed by hand. The deployment is now described using Ansible in https://gitlab.gnome.org/sthursfield/openqa-ansible.

The following steps from the initial deployment are not automated:

## Install docker
```
dnf install dnf-plugins-core	
dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
dnf install docker-ce docker-ce-cli containerd.io
```

## Downgrade containerd.io

```
dnf install containerd.io-1.4.6-3.1.fc34
```


## openqa will give admin access to the first user who loggedin. To give additional users admin access
```
docker exec -it db /bin/bash
psql -U openqa -d openqa
select * from users;
update users set is_admin=1, is_operator=1  where id=4;
```

## How to renew SSL cert

This should happen automatically, via the Caddy proxy.

## Fixing diskimage uploads

Uploading diskimages failed with error:

```
[2021-10-25T00:32:17.0733 UTC] [error] [pid:1057] Error uploading disk_iso_qemu_x86_64.qcow2: 500 response: Internal Server Error
[2021-10-25T00:32:17.0733 UTC] [error] [pid:1057] Upload failed for chunk 1
[2021-10-25T00:32:27.0495 UTC] [error] [pid:1057] Failed receiving asset: mkdir /var/lib/openqa/share/factory/tmp/private: Permission denied at /usr/lib/perl5/vendor_perl/5.26.1/Mojo/File.pm line 84.
```

Attempted fixed by running this inside container:

    chown geekotest /var/lib/openqa/factory/tmp

## Updating to latest OpenQA Web UI

The web UI is deployed from a fixed version of the upstream container image.

Instructions for updating the tag are in the Dockerfile: https://gitlab.gnome.org/sthursfield/openqa-ansible/-/blob/main/roles/openqa/files/Dockerfile

After updating the tag, rerun the Ansible playbook to deploy the new version.

## Updating to latest PostgreSQL

The major version of PostreSQL is fixed in the `Run openQA database container` job in [roles/openqa/tasks/main.yml](https://gitlab.gnome.org/sthursfield/openqa-ansible/-/blob/main/roles/openqa/tasks/main.yml). Periodically we need to check for new major PostreSQL releases and update this.

Major upgrades usually manually migrating the data: see https://www.postgresql.org/docs/current/upgrading.html for how to do this. I used `pg_dumpall` last time. Make sure to stop all containers and take a backup of the current database before you start the upgrade.

# openQA workers and gitlab-runners integration 
OpenQA is often used with dedicated worker machines, but we wanted to avoid creating a "pet" system that would require special maintenance. So after the CI pipeline has a Gitlab runner successfully build the artifacts, it then has another runner create an openQA worker instance with a unique ID. This ID ensures it can only be used by the pipeline that created it. The runner then issues a command to the openQA UI to perform the tests on its created worker, using API key and secret values configured in the Gitlab project.

You can see how everything fits together in this MR:
- https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1251/diffs

Current code implementing this at (It was moved from the original location):
- https://gitlab.gnome.org/GNOME/openqa-tests/-/blob/4b924e03d8e023c17cb83aef9cf3ed54ace9e75e/.gitlab-ci.yml#L24
- https://gitlab.gnome.org/GNOME/openqa-tests/-/tree/771c79f4076e7776f6dfbdad93435a361cfd002e/utils

# How to view logs

Logs are written to container stderr, so use `docker logs openqa_webui` to view them. (You might want `--tail 100` or `--follow` in there too).