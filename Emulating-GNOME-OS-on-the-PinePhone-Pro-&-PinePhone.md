## Dependencies

- QEMU 7.10
  - `apt-get autoremove --purge qemu-system\\\*`
  - `apt-get install -t bullseye-backports qemu-system`

  - `apt-get install qemu-system-arm`
  - `apt-get install qemu-efi-aarch64`
  - `apt-get install qemu-utils`

- Built AARCH64 GnomeOS image from [buildstream 1.6.8](https://docs.buildstream.build/1.6/index.html)

## Setup

1. cd into the checkout directory of your build or create a new directory with a downloaded AARCH64 `.img` file and cd into it

2. run the following commands to initialise the required files
   ```
   dd if=/dev/zero of=flash1.img bs=1M count=64
   dd if=/dev/zero of=flash0.img bs=1M count=64
   dd if=/usr/share/qemu-efi-aarch64/QEMU_EFI.fd of=flash0.img conv=notrunc
   ```

3. run the following qemu command to emulate the phone in terminal mode
```
qemu-system-aarch64 -nographic -machine virt,gic-version=max -m 4096M -cpu cortex-a72 -smp 8 \
-netdev user,id=vnet,hostfwd=:127.0.0.1:0-:22 -device virtio-net-pci,netdev=vnet \
-drive file=disk.img,format=raw,if=none,id=drive0,cache=writeback -device virtio-blk,drive=drive0,bootindex=0 \
-drive file=flash0.img,format=raw,if=pflash -drive file=flash1.img,format=raw,if=pflash
```

If you wish to emulate the system with minimal configurations i.e. without the overhead of the PinePhone Pro or PinePhone kernel configurations then:

- locate the linux.bst file within the relevant board
- update the configure command to `make defconfig`
- rebuild the image and follow the same instructions