### Intuition

We are going to be defining a manual element. Manual elements are described as:

- The most basic build element does nothing but allows users to add custom build commands to the array understood by the BuildElement

Additional reading link: https://docs.buildstream.build/1.6/elements/manual.html#module-elements.manual.

In effect, all our element will be doing is:

- Link to the source code, i.e., the raw C files
- Link to a makefile that runs commands to compile the file. These commands contain variables, i.e., destinations and prefixes for the final executable
- Link to a compiler that can run said makefile. In our case we will link to the GCC compiler available via Freedesktop SDK
- Run the commands using the config key that we will define
- Link everything we've done to a file relatively low in the dependency chain. This was explained in the general intuition section above.

### Commands to run

Run the following commands to create the necessary directory structure, assuming you are in the root directory of the GnomeOS project:

```
mkdir -p elements/custom/hello/src
```

Add these lines to define your `Makefile` in `hello/src`. As you can see when the compiler invokes the Makefile the DESTDIR and PREFIX variables must be passed in.

```
# Sample makefile for hello.c
#
.PHONY: all install

all: hello

install:
	install -d ${DESTDIR}${PREFIX}/bin
	install -m 755 hello ${DESTDIR}${PREFIX}/bin

hello: hello.c
	$(CC) -Wall -o $@ $<
```

Create a new C file, `hello.c` with these lines
```
#include <stdio.h>

int main(int argc, char *argv[])
{
  printf("Hello World\n");
  return 0;
}
```

Create our manual element which defines the commands that Buildstream should run processing this .bst file by adding these lines to `hello/hello.bst`.

```
kind: manual
description: |

  Building manually

# Depend on the base system
depends:
- freedesktop-sdk.bst:bootstrap-import.bst

# Stage the files/src directory for building
sources:
  - kind: local
    path: elements/custom/hello/src

# Now configure the commands to run
config:

  build-commands:
  - make PREFIX="%{prefix}"

  install-commands:
  - make -j1 PREFIX="%{prefix}" DESTDIR="%{install-root}" install
```

The final step is to link the hello.bst file we created into the dependency chain so that Buildstream is aware of our new file and can process the commands. As mentioned, we wish to define such a dependency very high in the chain so that prior cache can be used and that the majority of elements do not have to be rebuilt from cache. A recommended location is `deps.bst`.

Dependencies can be added like this:
```
- custom/hello/hello.bst
```

### Building and running the image

Build using the tutorial [here](Preliminary%20build%20instructions).

Run using the tutorial [here](Emulating GNOME OS)

You should be able to find the new executable inside the root directory of the OS inside the bin folder i.e. run the following inside GnomeOS.

```
cd /
./bin/hello
```