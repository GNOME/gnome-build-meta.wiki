For context and what we are missing for singed addons see: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/905

This is about adding an unsinged UKI addon that will append whatever `param` you need to the kernel in a persistent way.

- disable secureboot in the bios
- `sudo mkdir -p /boot/loader/addons`
- `ukify build --cmdline='<param>' --output ./param.addon.efi`
- `sudo mv ./param.addon.efi /boot/loader/addons`
- reboot and check the arguments they are reported in the journal

`/boot/` is the location of ESP, it could possibly be `/efi` depending on the installation.

`ESP/loader/addons` is the place to drop persistent addons, however you can also have per-version specific addons for example, `ESP/EFI/Linux/gnomeos_nightly.762980.efi.extra.d/param.addon.efi`