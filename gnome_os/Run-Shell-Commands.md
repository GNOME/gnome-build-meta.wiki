The standard GNOME console app is available in the base image, which is the simplest way to get a shell. You can also get an "emergency" shell in various ways, for cases where the desktop doesn't fully start.

## Emergency shell

You can add `emergency` or `rescue` to the kernel commandline to boot systemd into `emergency.target` or `rescue.target`. There is also the old trick of setting `init=/bin/sh`.

You can set kernel arguments using the SMBIOS, see the [Configure with SMBIOS](gnome_os/Configure-with-SMBIOS) guide.

## Debug shell

Add `systemd.debug_shell` to the kernel commandline to open a debug shell on virtual terminal 9. You can switch to this by sending CTRL+ALT+F9 (depending on how you connect to the VM you might have to use a special menu to send this key combination).

## During openQA tests

You can run the tests locally using the `ssam_openqa` tool. This allows you to pause the tests manually or on failure, and open a terminal connection or a VNC connection to the VM under test.
