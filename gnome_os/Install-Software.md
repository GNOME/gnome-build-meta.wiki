GNOME OS is not a general-purpose distribution and it doesn't support traditional package management.

There are several options available if you need programs that are not in the base image.

## Install apps with Flatpak

Flatpak is supported out of the box and you can use GNOME Software to install apps from Flathub or any other compatible app store.

## Install developer tools and system extensions with systemd-sysext

You can extend the base system with using [systemd-sysext](https://www.freedesktop.org/software/systemd/man/latest/systemd-sysext.html) format extensions.

There are a few ready made extensions for GNOME OS intended to be updated as part of the system. You can get the full list by running `updatectl features`, and enable or disable them using the `updatectl` tool. For example

```
sudo updatectl enable devel
```

Please note that this only enables feature, and it will be installed on the next `updatectl update`. You can pass `--now` to download the extension immediately.

The extensions that can be enabled this way are

* The `devel` extension, containing the full GNOME SDK and various other developer tools
* The `debug` extension contains debug info for everything in GNOME (note that it is _huge_)
* `snapd` (https://snapcraft.io/snapd)
* `apparmor`
* The `nvidia` proprietary drivers
