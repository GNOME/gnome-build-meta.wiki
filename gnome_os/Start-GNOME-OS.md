*As of April 2024 there are multiple variants of GNOME OS: the older `ostree` image and the newer `sysupdate` image, which supports Secure Boot. We are focusing development effort on the `sysupdate` image and this documentation assumes you're using that one.*

# openQA

GNOME OS is automatically tested on every commit using openQA, which boots the image on a Gitlab CI runner and runs the end-to-end tests. Depending on your use case, you might not need to run GNOME OS locally at all.

See [the openQA for GNOME developers](../openqa/OpenQA-for-GNOME-Developers) guide for more info.

# Local installation

## Getting the images

https://os.gnome.org/ is the main page of GNOME OS. It'll always link to the recommended variant of GNOME OS. Here you can find a list of all variants.

This is the "latest tested version" of GNOME OS, it may be a few weeks old but can be immediately updated to the latest nightly.
* https://os.gnome.org/download/latest/installer_x86_64.iso (ISO installer)
* https://os.gnome.org/download/latest/disk_ostree_x86_64.img.xz (ostree, raw disk image)
* https://os.gnome.org/download/latest/disk_sysupdate_x86_64.img.xz (sysupdate, raw disk image)

You can get an exact build of a specific pipeline (replace `$CI_PIPELINE_ID` with the appropriate pipeline id). The above "latest" links will redirect you to these, and you'll be able to see the pipeline id in the filename if your download software supports it (e.g. browsers generally do, `curl`/`wget` might need an additional option)
* `https://os.gnome.org/download/$CI_PIPELINE_ID/gnome_os_installer_$CI_PIPELINE_ID.iso` (ISO installer)
* `https://os.gnome.org/download/$CI_PIPELINE_ID/disk_$CI_PIPELINE_ID.img.xz` (ostree, raw disk image)
* `https://os.gnome.org/download/$CI_PIPELINE_ID/disk_sysupdate_$CI_PIPELINE_ID-x86_64.img.xz` (sysupdate, raw disk image)

## GNOME Boxes

We recommend using GNOME Boxes to install and run GNOME OS locally. You must install Boxes from Flathub. The version shipped with distros usually cannot run GNOME OS. See the [website](http://os.gnome.org) for instructions.

## QEMU

You can also call `qemu-system-x86_64` directly to start GNOME OS. See the [Bootable images in virtual machines](Bootable-images-in-virtual-machines) guide.

## Hardware

There is limited support for deploying GNOME OS to bare metal on x86\_64. Remember that GNOME OS comes with no guarantee of security and no warranty. Only use it for testing.

See the [website](http://os.gnome.org) for installation instructions.
