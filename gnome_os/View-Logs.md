Logs are recorded using `systemd-journald`. Here are some tips for using these logs to diagnose problems.

First, remember there are two journals: the system journal, and the *user* journal. Check both for problems -- you can use `journalctl --merge`to see both at once.

## Forward journal to console

Use the kernel argument `systemd.journald.forward_to_console=1` to forward the journal to the default console.

The openQA tests use this in combination with `console=ttyS0` to redirect the journal to the serial console. You can see this in the test artifacts in file `serial0`.

Remember you can set kernal arguments via the SMBIOS. See the [Configure with SMBIOS](Configure-with-SMBIOS) guide.

## Export logs to host machine

Reading logs inside your VM is not always convenient or possible. You can export all available journals to a text file as follows:

    journalctl --merge --output export > journal.exported

### Exporting logs from openQA

You can export logs as part of an openQA testsuite using the test API, as in this example:

    select_console('user-virtio-terminal');
    assert_script_run('journalctl --merge --output export | xz > /tmp/journal.xz');
    upload_asset('/tmp/journal.xz');
    select_console('x11');

The exported logs will be available as an asset named `journal.xz` associated with the test.

## Increasing log verbosity

There are ways to get more detailed logs from specific components. The exact way to do this depends on the component.

### systemd

systemd has a `log_level` setting which you can set to `debug`. There are several way to do this, one way is passing `systemd.log_level=debug` on the kernel commandline. See [systemd man page](https://www.freedesktop.org/software/systemd/man/latest/systemd.html) for more.

### GNOME modules

All GNOME components should use [GLib message logging](https://docs.gtk.org/glib/logging.html). By default, GLib drops debug and informational messages by default. Setting the environment variable `G_MESSAGES_DEBUG=all` causes all messages to be published.

In the case of a service managed by systemd, you can add `G_MESSAGES_DEBUG=all` to in service's `Environment=` setting so any debug messages are sent to the journal.
See the [Configure with SMBIOS guide](gnome_os/Configure-with-SMBIOS) for how to override systemd units at boot time.