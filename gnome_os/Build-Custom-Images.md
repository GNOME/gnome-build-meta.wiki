Building the image from source is relatively slow - there are several ways to
install software on top the base image without needing to rebuild. See the page
on how to [Install Software].

In certain cases you might need to build a custom base image though.

## Modifying the definitions

See the [BuildStream documentation](https://buildstream.build) for info on how GNOME OS is defined.

To add components to the VM, you would generally add them to `elements/vm/common-deps.bst`.

## Building in Gitlab CI

*As of April 2024, there is limited support for building Git branches directly in CI. At present, unless you push to a protected branch, images are built but not published. There is an open issue about this.*

## Building locally

Local builds of GNOME OS are documented in the gnome-build-meta `README.rst`.
