*There is work in progress to integrate systemd-sysupdate with GNOME Software so that updates can be installed that way.*

You can install updates, if available, using `systemd-sysupdate`. You can use the command line tool `updatectl` to update your system.

* Check for updates using `updatectl check`.
* Update your system using `updatectl update`.

Updates are built as part of the gnome-build-meta Gitlab CI pipelines in the `sysupdate-image` job, and published to the same location as the disk images.

  
Potential errors:

If the devel tree isn't mounted on reboot, your base system version probably doesn't match the devel systext version. Make sure you run `updatectl update` and then refresh/reboot.