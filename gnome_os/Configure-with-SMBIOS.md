GNOME OS is an "immutable" distribution with a read-only, cryptographically signed root filesystem. This means some more "traditional" debugging techniques for VMs, such as mounting the disk image and directly modifying files, are not possible here.

You can override system configuration by passing in credentials via the SMBIOS. Specifically credential "type 11" is read by the bootloader (systemd-boot) and init system (systemd).

## How to set SMBIOS credentials

This depends on how you deployed the machine (see: [Starting GNOME OS]).

### Set SMBIOS credentials with GNOME Boxes

FIXME: is this possible?

### Set SMBIOS credentials with QEMU

Use QEMU flag `-smbios type=11`. See the QEMU manual for details.

### Set SMBIOS credentials with openQA

Where machine type is `qemu`, the variable `QEMU_SMBIOS` controls the `-smbios` flag passed to QEMU. 

openQA only passes one `-smbios` flag but this can contain multiple comma-separated values and files. For example:

        QEMU_SMBIOS: type=11,value=io.systemd.credential.binary:tmpfiles.extra=...==,path=/tests/config/smbios.txt

### Viewing credentials in the system

You can confirm if a credential was correctly loaded. Open a shell (see the [Run Shell Commands guide](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/gnome_os/Run-Shell-Commands)), then run `systemd-creds --system list` as root to view all credentials, and use `systemd-creds --system cat` to see the value of a credential.

## Passing arguments to the kernel

You can set additional kernel flags using `io.systemd.stub.kernel-cmdline-extra` credential.

Here's an example that enables debug logs for systemd:

    io.systemd.stub.kernel-cmdline-extra=systemd.log_level=debug

## Creating files in /etc

You can use the `tmpfiles.extra` credential to create small files inside /etc early in the boot process. This can be useful to change and debug the boot process. 

Here's an example of overriding a systemd service by creating a definition of it in /etc:

 1. Define the contents of the file.

        $ edit ./example.service

 2. Calculate its base64 encoding:

        $ base64 -w 0 < example.service

 3. Create a systemd-tmpfiles configuration to create that file.

        $ cat > tmpfiles.txt
        f+~   /etc/systemd/system/example.service   644 root    root    -   <base64 contents>

 4. Base64 encode your tmpfiles configuration:

        $ base64 -w 0 < tmpfiles.txt

 5. Pass the resulting base64 string as `io.systemd.credential.binary:tmpfiles.extra=<base64 contents>`

The [systemd.unit man page](https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html) specifies that a "drop-in" directory foo.service.d/ may exist which can override unit configuration. So far I've never got this to usefully work.

## Further reading

See the systemd documentation for a full list of things you can do with credentials.
