## Instructions for QEMU

### Build

```
bst -o arch riscv64 build iso/image.bst freedesktop-sdk.bst:components/opensbi-u-boot.bst
bst -o arch riscv64 checkout iso/image.bst iso
bst -o arch riscv64 checkout freedesktop-sdk.bst:components/opensbi-u-boot.bst firmware
```

### Install

```
qemu-img create -f qcow2 disk.qcow2 50G
qemu-system-riscv64 -M virt -m 16G -smp 8 -bios firmware/usr/share/opensbi/lp64/generic/firmware/fw_payload.elf -drive file=iso/installer.iso,format=raw,media=cdrom,if=virtio -drive file=disk.qcow2,format=qcow2,if=virtio -display spice-app,gl=on -device virtio-gpu,xres=1920,yres=1080 -device usb-ehci -device usb-kbd -device usb-tablet -netdev user,id=n1 -device virtio-net-pci,netdev=n1 -soundhw hda
```

### Alternative to installation

```
bst -o arch riscv64 checkout vm/image.bst vm
unxz vm/disk.img.xz
qemu-img convert vm/disk.img -O qcow2 disk.qcow2
qemu-img resize disk.qcow2 50G
```

### Run

```
qemu-system-riscv64 -M virt -m 16G -smp 8 -bios firmware/usr/share/opensbi/lp64/generic/firmware/fw_payload.elf -drive file=disk.qcow2,format=qcow2,if=virtio -display spice-app,gl=on -device virtio-gpu,xres=1920,yres=1080 -device usb-ehci -device usb-kbd -device usb-tablet -netdev user,id=n1 -device virtio-net-pci,netdev=n1 -soundhw hda
```

## Instructions for SiFive Unmatched

As above, but following [!5046](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/5046/):

* Build and flash `vm/firmware/bootloader-unmatched.bst` to an sd-card. Put it in the sd card reader of the board.
* Flash `vm/minimal/efi.bst` or `vm/desktop/efi.bst` to NVMe or to a USB storage device. And add the device to the board.
* Press the power button.