[Bootable images in virtual machines](Bootable-images-in-virtual-machines)

[Home](Home)

[Infrastructure](Infrastructure)

[Pinebook Pro image](Pinebook-Pro-image)

[PinePhone Pro & PinePhone images](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/PinePhone-Pro-&-PinePhone-images)

[Release Contents](Release-Contents)

[deploy components ostree](deploy-components-ostree)

openqa
- [Deployment](openqa/Deployment)
- [OpenQA for GNOME developers](openqa/OpenQA-for-GNOME-developers)

[Sysupdate MOK builds](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/Locally-build-and-sign-GNOME-OS-image-with-tpm2-and-MOK)

[risc v](risc-v)