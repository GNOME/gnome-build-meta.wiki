On an AARCH64 machine such as an Nvidia Jetson:

## PinePhone Pro image
```
bst build boards/pinephone-pro/image.bst
bst checkout boards/pinephone-pro/image.bst checkout
```

## PinePhone image
```
bst build boards/pinephone/image.bst
bst checkout boards/pinephone/image.bst checkout
```