| Machine | Purpose | Location  | Architecture | Access | Comments |
| ------ | ------ | ------ | ------ | ------ | ------ |
| gbm-builder.gnome.org | hosts nightly builds of flatpak runtimes and apps | GNOME | x86_64 | Infra team | |
| 167.99.17.106 | remote cache server | GNOME DigitalOcean | x86_64 | jjardon, av | |
| 147.75.194.17 | runner | packet.net | x86_64  | infra team, jjardon | m1.xlarge.x86 machine donated by https://packet.net |
| 147.75.198.26 | runner | packet.net | aarch64 | infra team, jjardon | c1.large.arm machine donated by https://www.worksonarm.com/ |
| 147.75.74.6 | runner | packet.net | aarch64(armv7) | infra team, jjardon | c1.large.arm.xda machine Donated by https://www.codethink.co.uk/ |
| openqa.gnome.org | OpenQA web UI | GNOME | x86_64 | jjardon, av, Codethink | |


See also: [OpenQA deployment](../openqa/Deployment)
