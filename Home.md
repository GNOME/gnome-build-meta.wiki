# Release documentation
- [Release Contents](Release-Contents)

# GNOME OS Bootable images
- Nightly images [os.gnome.org](https://os.gnome.org/)
- [GNOME OS Documentation](gnome_os)
- Automated testing of the images:
    - [Using OpenQA](openqa/OpenQA-for-GNOME-developers)
- [Documentation about bootable images for virtual machines](Bootable-images-in-virtual-machines)
- [Documentation about bootable image for Pinebook Pro](Pinebook-Pro-image)
- [How to build your own components and deploy on the image using ostree](deploy-components-ostree)
- [Build for RISC-V](risc-v)
- [Documentation about bootable image for PinePhone Pro & PinePhone](PinePhone-Pro-&-PinePhone-images)
- [Setup home encryption](GNOME-OS-home-encryption)

## GNOME OS - Latest versions:

  - x86_64:
    - master: https://os.gnome.org/download/latest/gnome_os_installer.iso
    - gnome-41:
      - GNOME 41.0: https://os.gnome.org/download/41.0/gnome_os_installer_41.0.iso
    - gnome-40:
      - GNOME 40.4: https://os.gnome.org/download/40.4/gnome_os_installer_40.4.iso
  - ARM64 (pinebook pro):
    - master: Work in progress, see: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/857
  - ARM64 (pinephone)
    - master: Work in progress, see: !1727

## Get Involved
- https://matrix.to/#/#gnome-os:gnome.org

# Infrastructure
- [Infrastructure](Infrastructure)

# Technical conditions to enter GNOME core:
 - See [app review criteria](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md#core-app-and-development-tool-criteria)

## Process to add components to the SDK
- Create MR and be sure at least other 3 members of the release team approve (thumbs up) it

## Process to remove components from SDK
In general, for libraries used by several modules, the removal process will last for 2 cycles; for example announce intention for 41, remove for 42 (if other preconditions are met)
- Create MR and be sure at least other 3 members of the release team approve (thumbs up) it
- Notify the maintainer about the removal plans
- Notify the community about the removal so they have time to port
- When core apps have been ported / enough time has pass, merge the MR
