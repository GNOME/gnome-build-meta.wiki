This is a small guide to setup home encryption in [GNOME OS](https://os.gnome.org/).

:warning: consider this experimental and unsupported

We will make use of [systemd-homed](https://systemd.io/HOME_DIRECTORY/). It is not perfectly supported across the GNOME stack. Here is an example creating a new hypothetical "Jay Doe" administor user.

```sh
export username=jay
sudo homectl create $username --storage=luks --disk-size=50G
sudo homectl update -member-of=wheel $username
sudo homectl update --nosuid=false --nodev=false --noexec=false $username
sudo usermod --add-subuids 524288-1878982656 $username
sudo usermod --add-subgids 524288-1878982656 $username
```

If your new user doesn't appear in GDM, select "Not listed?" and enter username manually.


If things go wrong, you can get a debug shell and switch to tt9 to get a root shell. See [Debugging a failed boot](Bootable-images-in-virtual-machines#debugging-a-failed-boot)

TODO:

- [ ] Figure out why login is not possible without `--disk-size=50G` https://gitlab.gnome.org/Teams/Releng/gnome-os-site/-/merge_requests/8/diffs#note_1688606
- [ ] Find a way to better allocate uids https://gitlab.gnome.org/Teams/Releng/gnome-os-site/-/merge_requests/8/diffs#note_1688607
- [ ] Document how to decrypt / mount the home dir via CLI