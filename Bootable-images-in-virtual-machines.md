# Bootable images

## Building the installer

Build `iso/image.bst`. Its artifact will contain `installer.iso`

You will need to install BuildStream for this, check instructions at https://buildstream.build/install.html

```
bst build iso/image.bst
bst checkout iso/image.bst checkout
```

## Running with GNOME Boxes

If GNOME Boxes does not have osinfo entry, if using an updated version of the osinfo entry, you may want to add it manually. See the [related section](#osinfo)

Create a virtual machine using an "Operating System Image File". Select `installer.iso` from the checkout directory.

## Enable the boot menu:

In the libvirt config, can be opened from the boxes properties, add the following:

```xml
<os>
  <bootmenu enable="yes"/> 
</os>
```

## Update from the terminal

Use stock ostree commands:

```sh
ostree admin upgrade
```

## Switch to the development branch

Open a terminal and type:

```sh
ostree admin switch gnome-os:gnome-os/master/x86_64-devel
```

## Testing stuff with sytemd-sysext

The devel tree has the compiler, headers and .pc files for everything, however /usr is still not writtable. Fear not we can still compile and test stuff on the host with `systemd-sysext`! See Lennart's [blogpost](https://0pointer.net/blog/testing-my-system-code-in-usr-without-modifying-usr.html) and the `systemd-sysext manpages`

One time setup:

```
sudo mkdir -p /var/lib/extensions/shell-test/usr/lib/extension-release.d/
sudo cp /etc/os-release /var/lib/extensions/shell-test/usr/lib/extension-release.d/extension-release.shell-test
```

```
meson setup build && meson compile -C build
sudo DESTDIR=/var/lib/extensions/shell-test meson install -C build --quiet --no-rebuild 
# Or with --force to avoid setting up the extension-release file
sudo systemd-sysext refresh
```



## Updating with local OSTree

Run helper script: `utils/update-local-repo.sh`. This will create a local repository with the current state of working copy.

Then run `utils/run-local-repo.sh` to start a server. This script does not fork. Leave it to run.

Open a shell and type `enable-developer-repository`. The server has to be running at that time. You do not need to pass any parameter if you are running the image in a QEMU with standard configuration for user network (`-netdev user`). For other configuration look at configuration with `--help`.

If `enable-developer-repository` succeeded, you can then reboot. Do not call `enable-developer-repository` again. Further updates will be done automatically by `eos-updater`. To force update sooner, run `eos-updater-ctl update`, or use GNOME Software.

## Always enable the systemd debug shell

This can be done by editing the kernel cli arguments, and then switching to TTY 9, there's a shortcut for the tty in Boxes!

```sh
sudo ostree admin deploy --karg-append=systemd.debug_shell=1 gnome-os:gnome-os/master/x86_64-devel
```

## Trigger Initial Setup with an existing session

This can be done by adding `gnome.initial-setup=1` to the kernel cli args. Alternatively you can also delete the existing users in a system with `userdel [user] -r` and then reboot. Not that you will want to have the debug_shell or equivelant enable in order to inspect the system without a user present.


## Adding osinfo entry for GNOME Boxes

If you need to update the osinfo entry for GNOME Boxes, copy file `gnome-os-master.xml` to `$XDG_CONFIG_HOME/osinfo/os/gnome.org/gnome-os-master.xml` for GNOME Boxes.

Using GNOME Boxes devel on flatpak, it will be: `\~/.var/app/org.gnome.BoxesDevel/config/osinfo/os/gnome.org/gnome-os-master.xml`.

Using GNOME Boxes stable on flatpak, it will be: `\~/.var/app/org.gnome.Boxes/config/osinfo/os/gnome.org/gnome-os-master.xml`.

Otherwise, `\~/.config/osinfo/os/gnome.org/gnome-os-master.xml`

## Building the base image (not installer)

Build `vm/image.bst` and checkout. The image will be `disk.img.xz`, raw format (xz compressed).

```sh
bst build vm/image.bst
bst artifact checkout vm/image.bst
```
Uncompress the image, to get `disk.img`.

```sh
unxz disk.img.xz
```
## Running manually with QEMU the base image

Before the first run, copy the UEFI variables image.

```sh
cp /usr/share/OVMF/OVMF_VARS.fd .
```

Then, run with QEMU:

```sh
qemu-system-x86_64 \
  -enable-kvm -m 4G -smp 4 -machine q35,accel=kvm -cpu host \
  -drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
  -drive if=pflash,format=raw,unit=1,file=OVMF_VARS.fd \
  -display gtk,gl=on -vga virtio \
  -netdev user,id=net1 -device e1000,netdev=net1 \
  -audiodev none,id=snd0 -device intel-hda -device hda-output,audiodev=snd0 \
  -usb -device usb-tablet \
  -drive file=vm/image/disk.img,format=raw,media=disk
```

For more explanations on the QEMU command line see section [Using QEMU](#using-qemu).

## Running manually with QEMU the installer

Before the first run, copy the UEFI variables image.

```sh
cp /usr/share/OVMF/OVMF_VARS.fd .
```

And create a disk:

```sh
qemu-img create -f qcow2 disk.qcow2 64G
```

Then run with QEMU:

```sh
qemu-system-x86_64 \
  -enable-kvm -m 4G -smp 4 -machine q35,accel=kvm -cpu host \
  -drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
  -drive if=pflash,format=raw,unit=1,file=OVMF_VARS.fd \
  -display gtk,gl=on -vga virtio \
  -netdev user,id=net1 -device e1000,netdev=net1 \
  -audiodev none,id=snd0 -device intel-hda -device hda-output,audiodev=snd0 \
  -usb -device usb-tablet \
  -drive file=disk.qcow2,format=qcow2,media=disk \
  -drive file=checkout/installer.iso,format=raw,media=cdrom
```

After installation re-launch QEMU, but remove the last drive (CD-ROM).

```sh
qemu-system-x86_64 \
  -enable-kvm -m 4G -smp 4 -machine q35,accel=kvm -cpu host \
  -drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
  -drive if=pflash,format=raw,unit=1,file=OVMF_VARS.fd \
  -display gtk,gl=on -vga virtio \
  -netdev user,id=net1 -device e1000,netdev=net1 \
  -audiodev none,id=snd0 -device intel-hda -device hda-output,audiodev=snd0 \
  -usb -device usb-tablet \
  -drive file=disk.qcow2,format=qcow2,media=disk
```

### Using QEMU

#### UEFI boot

You need two files, OVMF/EDK2 code and variables.

We assume here we are using x86_64. The paths are:

- For vanilla QEMU:
  * /usr/share/qemu/edk2-x86_64-code.fd
  * /usr/share/qemu/edk2-i386-vars.fd
- For Debian and Fedora:
  * /usr/share/OVMF/OVMF_CODE.fd
  * /usr/share/OVMF/OVMF_VARS.fd

Copy the variable file locally. It is needed to be modified. The code can stay because it will be read only.

#### Creating a hard disk

To create a 64GB disk, for example:

```sh
qemu-img create -f qcow2 disk.qcow2 64G
```

#### QEMU Parameters

4 CPU threads: `-smp 4`

4GB memory: `-m 4G`

For x86_64 with KVM: `-enable-kvm -machine q35,accel=kvm`

Use same CPU architecture as host: `-cpu host` (the default can be a basic one that doesn't support all instructions modern compilers use, leading to SIGILL crashes)

UEFI boot:

```sh
-drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on
-drive if=pflash,format=raw,unit=1,file=OVMF_VARS.fd
```

Enabling graphics hardware acceleration: `-display gtk,gl=on -vga virtio`

Enabling sound: `-audiodev none,id=snd0 -device intel-hda -device hda-output,audiodev=snd0`

Enabling network: `-netdev user,id=net1 -device e1000,netdev=net1`

Getting the mouse pointer to work in windowed mode: `-usb -device usb-tablet`. Alternatively, you can use `-fullscreen`.

Adding a hard drive: `-drive file=disk.qcow2,format=qcow2,media=disk`

Adding a CD-ROM: `-drive file=cd.iso,format=raw,media=cdrom`

#### Using SPICE protocol instead of Gtk interface

Instead of `-display gtk,gl=on`, one can use:

`-device virtio-serial-pci -device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 -chardev spicevmc,id=spicechannel0,name=vdagent -spice "addr=${XDG_RUNTIME_DIR:-/tmp}/vm.sock,unix,disable-ticketing,gl=on"`

Then you can `spice+unix://${XDG_RUNTIME_DIR:-/tmp}/vm.sock`

## Appendix

### Debugging a failed boot

The bootloader is [systemd-boot](https://www.freedesktop.org/wiki/Software/systemd/systemd-boot/), you can press `e` at the menu to edit kernel commandline arguments. If you get a black screen on boot, try removing `quiet` and `splash` arguments.

Set `console=ttyS0` kernel argument to enable login on the serial console. If running `qemu` from a terminal, you can pass `-serial stdio` to QEMU to connect serial console to your terminal, which allows you to copy and paste.

There is no root password set by default. You can get a login shell by adding kernel argument `systemd.debug_shell=1`, then switching to VT9 by pressing `<ctrl>+<alt>+<F9>`.

Another way to change shell is to drop to a text tty, and then use `<alt>+<F9>`

### nr_entries is too big

GNOME Shell seems to hit a limit in QEMU. If you get this error message in the standard error stream of QEMU, try the following patch:

```diff
Index: qemu-4.1/hw/display/virtio-gpu.c
===================================================================
--- qemu-4.1.orig/hw/display/virtio-gpu.c
+++ qemu-4.1/hw/display/virtio-gpu.c
@@ -616,7 +616,7 @@ int virtio_gpu_create_mapping_iov(VirtIO
     size_t esize, s;
     int i;
 
-    if (ab->nr_entries > 16384) {
+    if (ab->nr_entries > (256*1024)) {
         qemu_log_mask(LOG_GUEST_ERROR,
                       "%s: nr_entries is too big (%d > 16384)\n",
                       __func__, ab->nr_entries);
```