# Pine book

The [Pinebook Pro](https://wiki.pine64.org/index.php/Pinebook_Pro) is a $200 arm 64 laptop from PINE64.

You will need to install BuildStream for build locally, check instructions at https://buildstream.build/install.html

Build `boards/pinebook-pro/image.bst`. Checkout the result

In the artifact, there will be a file named `disk.qcow2`. You can flash it to an sd card or an eMMC module using `qemu-img dd if=disk.qcow2 of=/dev/device-to-which-to-write`.

If you do not get anything to boot, I recommend you use the serial port of the computer to debug the issue.

## Updating the keyboard/trackpad firmware

Some first batches of the Pinebook Pro require a firmware update. Unfortunately, there is no automatic way to do it at the moment.

If your keyboard is an ISO keyboard, do the following.

```
$ sudo pinebook-pro-keyboard-updater step-1 iso
```

Then reboot. After reboot:

```
$ sudo pinebook-pro-keyboard-updater step-2 iso
```

Then reboot one more time. The firmware is updated.

If you have an ANSI keyboard, replace `iso` by `ansi` in the command line.